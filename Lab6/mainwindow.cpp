#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

void MainWindow::connectToDatabase(QString databaseName) {
    QString dbPath = QDir::toNativeSeparators(qApp->applicationDirPath() + "/" + databaseName);
    sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName(dbPath);

    if (!sdb.open()) {
        QMessageBox::critical(this, tr("Database Connection failed"), tr("Unable to connect to the database"));
        exit(1);
    }
}

void MainWindow::connectTableModelToWidgetView() {
    studentTableModel = new QSqlTableModel(ui->studentView);
    studentTableModel->setTable("Student");
    studentTableModel->setEditStrategy(QSqlTableModel::OnFieldChange);
    studentTableModel->select();
    ui->studentView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->studentView->setModel(studentTableModel);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connectToDatabase("lab6.db");
    connectTableModelToWidgetView();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clearAllFields() {
    ui->idField->setText("");
    ui->nameField->setText("");
    ui->gpaField->setText("");
    ui->educationFormField->setText("");
    ui->groupIdField->setText("");
}

bool MainWindow::insertNewStudent(QString id, QString name, float gpa, QString educationForm, int groupId) {
    QSqlRecord newStudentRecord = studentTableModel->record();
    newStudentRecord.setValue("Id", id);
    newStudentRecord.setValue("Name", name);
    newStudentRecord.setValue("GPA", gpa);
    newStudentRecord.setValue("EducForm", educationForm);
    newStudentRecord.setValue("GroupId", groupId);

    return studentTableModel->insertRecord(-1, newStudentRecord) &&
           studentTableModel->submitAll();
}

void MainWindow::on_addStudentButton_released()
{
    bool gpaConversionIsOkay = false;
    bool groupIdConversionIsOkay = false;

    QString id = ui->idField->text();
    QString name = ui->nameField->text();
    float gpa = ui->gpaField->text().toFloat(&gpaConversionIsOkay);
    QString educationForm = ui->educationFormField->text();
    int groupId = ui->groupIdField->text().toInt(&groupIdConversionIsOkay);

    if (!gpaConversionIsOkay || !groupIdConversionIsOkay || gpa > 5.0 || name.length() == 0) {
        QMessageBox::critical(this, tr("Invalid data"), tr("Make sure GPA is a float and Group ID is an int and have correct values"));
        return;
    }

    bool insertWasSuccessful = insertNewStudent(id, name, gpa, educationForm, groupId);

    if (insertWasSuccessful) {
        clearAllFields();
    } else {
        QMessageBox::critical(this, tr("Unable to add a student"), tr("There has been an error trying to add a student. That's all we know."));
        return;
    }
}

