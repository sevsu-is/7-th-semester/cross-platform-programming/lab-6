#pragma once
#include <QMainWindow>
#include <QtSql>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_addStudentButton_released();

private:
    Ui::MainWindow *ui;
    QSqlDatabase sdb;
    QSqlTableModel* studentTableModel;
    void connectToDatabase(QString databaseName);
    void connectTableModelToWidgetView();
    void clearAllFields();
    bool insertNewStudent(QString id, QString name, float gpa, QString educationForm, int groupId);
};
