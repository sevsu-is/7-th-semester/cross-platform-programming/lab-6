/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.10
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLabel *addStudentLabel;
    QFormLayout *formLayout;
    QLineEdit *idField;
    QLabel *nameLabel;
    QLineEdit *nameField;
    QLabel *gpaLabel;
    QLineEdit *gpaField;
    QLabel *label;
    QLineEdit *educationFormField;
    QLabel *groupIdLabel;
    QLineEdit *groupIdField;
    QLabel *idLabel;
    QPushButton *addStudentButton;
    QTableView *studentView;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        addStudentLabel = new QLabel(centralwidget);
        addStudentLabel->setObjectName(QString::fromUtf8("addStudentLabel"));
        QFont font;
        font.setBold(true);
        addStudentLabel->setFont(font);

        verticalLayout->addWidget(addStudentLabel);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        idField = new QLineEdit(centralwidget);
        idField->setObjectName(QString::fromUtf8("idField"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(idField->sizePolicy().hasHeightForWidth());
        idField->setSizePolicy(sizePolicy);

        formLayout->setWidget(0, QFormLayout::FieldRole, idField);

        nameLabel = new QLabel(centralwidget);
        nameLabel->setObjectName(QString::fromUtf8("nameLabel"));

        formLayout->setWidget(1, QFormLayout::LabelRole, nameLabel);

        nameField = new QLineEdit(centralwidget);
        nameField->setObjectName(QString::fromUtf8("nameField"));
        sizePolicy.setHeightForWidth(nameField->sizePolicy().hasHeightForWidth());
        nameField->setSizePolicy(sizePolicy);

        formLayout->setWidget(1, QFormLayout::FieldRole, nameField);

        gpaLabel = new QLabel(centralwidget);
        gpaLabel->setObjectName(QString::fromUtf8("gpaLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, gpaLabel);

        gpaField = new QLineEdit(centralwidget);
        gpaField->setObjectName(QString::fromUtf8("gpaField"));
        sizePolicy.setHeightForWidth(gpaField->sizePolicy().hasHeightForWidth());
        gpaField->setSizePolicy(sizePolicy);

        formLayout->setWidget(2, QFormLayout::FieldRole, gpaField);

        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label);

        educationFormField = new QLineEdit(centralwidget);
        educationFormField->setObjectName(QString::fromUtf8("educationFormField"));
        sizePolicy.setHeightForWidth(educationFormField->sizePolicy().hasHeightForWidth());
        educationFormField->setSizePolicy(sizePolicy);

        formLayout->setWidget(3, QFormLayout::FieldRole, educationFormField);

        groupIdLabel = new QLabel(centralwidget);
        groupIdLabel->setObjectName(QString::fromUtf8("groupIdLabel"));

        formLayout->setWidget(4, QFormLayout::LabelRole, groupIdLabel);

        groupIdField = new QLineEdit(centralwidget);
        groupIdField->setObjectName(QString::fromUtf8("groupIdField"));
        sizePolicy.setHeightForWidth(groupIdField->sizePolicy().hasHeightForWidth());
        groupIdField->setSizePolicy(sizePolicy);

        formLayout->setWidget(4, QFormLayout::FieldRole, groupIdField);

        idLabel = new QLabel(centralwidget);
        idLabel->setObjectName(QString::fromUtf8("idLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, idLabel);


        verticalLayout->addLayout(formLayout);

        addStudentButton = new QPushButton(centralwidget);
        addStudentButton->setObjectName(QString::fromUtf8("addStudentButton"));
        sizePolicy.setHeightForWidth(addStudentButton->sizePolicy().hasHeightForWidth());
        addStudentButton->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(addStudentButton);

        studentView = new QTableView(centralwidget);
        studentView->setObjectName(QString::fromUtf8("studentView"));
        studentView->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(studentView->sizePolicy().hasHeightForWidth());
        studentView->setSizePolicy(sizePolicy1);
        studentView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

        verticalLayout->addWidget(studentView);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 28));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Student Adder", nullptr));
        addStudentLabel->setText(QCoreApplication::translate("MainWindow", "Add Student:", nullptr));
        nameLabel->setText(QCoreApplication::translate("MainWindow", "Name:", nullptr));
        gpaLabel->setText(QCoreApplication::translate("MainWindow", "GPA:", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Education Form:", nullptr));
        groupIdLabel->setText(QCoreApplication::translate("MainWindow", "Group ID:", nullptr));
        idLabel->setText(QCoreApplication::translate("MainWindow", "ID:", nullptr));
        addStudentButton->setText(QCoreApplication::translate("MainWindow", "Add", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
